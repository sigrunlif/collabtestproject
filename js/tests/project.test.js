document.body.innerHTML = '<h1>Git Collab Project</h1><div id="characters">Hér koma karakterar</div>'
var { insertName, insertImage, makeDiv, renderCharacter } = require('../main.js');

test('The image URL should contain "http"', function() {
    for(var i = 0; insertImage(i); i++)
    expect( insertImage(i) ).toMatch(/http/); 
}); 

test('The first letter of the name should be capital', function() {
    var getFirstLetter = function(x) {
        return x[0]
    }
    for(var i = 0; insertName(i); i++){
        expect(getFirstLetter(insertName(i))).toBe(getFirstLetter(insertName(i).toUpperCase()))
    }
}); 

test('The div tag must contain <h2> and an <img> tag', function(){
    for(var i = 0; makeDiv(i); i++)
    expect( makeDiv(i) ).toMatch(/(<h2>|<img([\w\W]+?)\/>)/);
});
