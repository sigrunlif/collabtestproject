
var insertName = function(n) {
    var names = [
        "Kötturinn Njáll",
        "Pósturinn Páll"
    ]
    return names[n]
}

var insertImage = function(n) {
    var images = [
        "http://www.newrivercabinetry.com/wp-content/uploads/2018/12/all-about-tuxedo-cats-facts-lifespan-and-intelligence-petite-black-white-cat-valuable-4.jpg",
        "http://svarthofdi.is/assets/images/news/642/postmanpat-movie-trailer.jpg"
    ]
    return images[n] 
}

var makeDiv = function(n) {
    if(!insertName(n)) {
        return false
    }
    var div = `
        <h2>${insertName(n)}</h2>
        <img src="${insertImage(n)}">
    `
    return div; 
}

var renderCharacter = function(n) {
    if(insertName(n)) {
        document.querySelector("#characters").innerHTML = makeDiv(n);
    }
}

renderCharacter(0); 

module.exports = {
    insertName,
    insertImage,
    makeDiv,
    renderCharacter,
}

